import sqlite3 as lite
import pandas as pd
from sbmtm import sbmtm
import pickle
from datetime import datetime

def main (model):
    print ("loading model")
    t_start = datetime.now()

    with open(model, 'rb') as model_file:
        model = pickle.load(model_file)
    duration =  datetime.now()-t_start

    print ("model loaded in {} seconds".format(duration.seconds))
    topic_model (model)


def topic_model (model):
    from pprint import pprint
    con = lite.connect ("./corpus.db")
    executor = "select * from cases"
    df = pd.read_sql_query (executor, con)
    
    t_start = datetime.now()
    pprint (model.topics(l=1,n=10)) 
    duration =  datetime.now()-t_start
    print ("printing topics took {} seconds".format(duration.seconds))

    for index, row in df.iterrows():
        print (row["tweet"], "\n")
        
        t_start = datetime.now()
        tops = model.topicdist(index,l=1)
        tops.sort (key=lambda x: x[1], reverse = True)
        pprint (tops)
        duration =  datetime.now()-t_start
        print ("finding document topics took {} seconds".format (duration.seconds))
        input ("next?")

if __name__ == "__main__":
    model = "sbm_weibo_100k"
    main (model)
